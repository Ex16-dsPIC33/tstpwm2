/*! \file ADC1Initialize.c
 *  \brief Initialize A/D converter
 *
 * Set A/D converter to repeatedly read channel 4 and
 * interrupt when conversion is complete.
 */
#include <p33Fxxxx.h>

//! Initialize A/D converter
/*!
Pseudocode:
\code
Set AN4 and 5 to analog
Set auto sampling and convert (AC1CON1)
Set 12-bit 1 channel (AD1CON1.AD12B)
Use MUX A, 1 per interrupt, Vdd/Vss ref (AD1CON2)
Set samples and bit conversion time (AD1CON3)
Set channel scanning time (AD1CSSL)
Select channel AN5 (AD1CHS0)
Reset interrupt flag (AD1IF)
Enable ADC interrupts (AD1IE)
Turn on ADC (ADON)
\endcode
*/
void ADC1Initialize( void )
{
    /* set port configuration here */
    AD1PCFGLbits.PCFG4 = 0;         // ensure AN4/RB4 is analog (Temp Sensor)
    AD1PCFGLbits.PCFG5 = 0;         // ensure AN5/RB5 is analog (Analog Pot)
    /* set channel scanning here, auto sampling and convert,
       with default read-format mode */
    AD1CON1 = 0x00E4;
    /* select 12-bit, 1 channel ADC operation */
    AD1CON1bits.AD12B = 1;
    /* No channel scan for CH0+, Use MUX A,
       SMPI = 1 per interrupt, Vref = AVdd/AVss */
    AD1CON2 = 0x0000;
    /* Set Samples and bit conversion time */
    AD1CON3 = 0x032F;
    /* set channel scanning here for AN4 and AN5 */
    AD1CSSL = 0x0000;
    /* channel select AN5/RB5 */
    AD1CHS0 = 0x0005;
    /* reset ADC interrupt flag */
    IFS0bits.AD1IF = 0;
    /* enable ADC interrupts */
    IEC0bits.AD1IE = 1;
    /* turn on ADC module */
    AD1CON1bits.ADON = 1;
}
