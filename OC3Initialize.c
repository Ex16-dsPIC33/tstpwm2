/*! \file OC3Initialize.c
    \brief Initialize the OC3 PWM device
*/
#include <p33Fxxxx.h>
#include "Constants.h"

/*! Initialize OC3 for PWM
 * Timer 3 is set up and the PWM on OC3 is initialized
 * to use Timer 3.
 *
 * Pseudocode:
 * \code
 *   Clear TMR3
 *   Set the Timer 3 counter to 1000
 *   Set the initial PWM 3 duty cycle to OC3INIT
 *   Set up PWM 3 (OC3) to use Timer 3
 *   Set Timer 3 to 1:4 prescaler, use clock and start the timer
 * \endcode
 */
void OC3Initialize( void )
{
    // Set up PWM on OC3 (RD2)
    TMR3 = 0;             // Clear timer 3
    PR3 = 1000;           // Timer 3 counter to 1000
    OC3RS = OC3INIT;      // PWM 3 duty cycle
    OC3R = OC3INIT;       //
    OC3CON = 0xe;         // Set OC3 to PWM mode, timer 3
    T3CON = 0x8010;       // Fosc/4, 1:4 prescale, start TMR3
}
