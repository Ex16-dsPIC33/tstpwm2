/*! \file T1Interrupt.c
    \brief Timer 1 interrupt service routine
*/
#include <p33Fxxxx.h>

//! Timer 1 interrupt service routine
/*! Whenever Timer1 expores this routine is called.  The routine
    resets the interupt flag and toggles LED D3.
*/
void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF = 0;    // clear timer interrupt flag
    LATA ^= 0x01;         //Toggle LED D3 (RA0)
}
