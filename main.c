/*! \file main.c
 *
 * \brief Test PWM on dsPIC33FJ256GP710
 *
 * This is a walk and chew gum sort of an exercise.  LED's
 * D4-D9 get toggled frequently based on the loop counter.
 * D3 gets toggled based on Timer 1.
 *
 * Timer 3 is used to control the PWM on OC3.  The duty cycle is
 * slowly increased, causing a LED on an external board to slowly
 * become dimmer.  When the duty cycle gets to 100%, LED D10 is
 * toggled, and the duty cycle gets reset to 0.
 *
 * Meanwhile, OC7 is being used to control another external
 * LED.  This LED's duty cycle is based on the position of the
 * onboard potentiometer.
 *
 * The A/D converter is set to read the potentiometer continuously
 * and interrupt whenever the conversion is complete.  The A/D
 * ISR simply grabs the value and stuffs it in a global to be used
 * later by OC3.
 *
 * jjmcd 2011-06-17
 */

#include <stdlib.h>
#include <p33Fxxxx.h>
#include "Constants.h"

//! Macro to set select oscillator fuse
_FOSCSEL(FNOSC_PRIPLL);
//! Macro to set oscillator fuses
_FOSC(FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMD_XT);
//! Macro to turn off watchdog timer
_FWDT(FWDTEN_OFF);

//! Storage for input analog value
volatile unsigned int potValue;
//! Remember lowest A/D value seen
unsigned int lowValue;
//! Remember highest A/D value seen
unsigned int highValue;

//! main - mainline for tstPWM2
/*!

main() first initializes the ports then goes into an infinite
loop.  During the loop, delayCounter is decremented.  When it
reaches zero, LEDs D4-D9 are toggled.  Each time through the loop
the A/D result is examined and the highest and lowest values ever
seen are remembered (for the in-circuit debugger).  Finally, the
analog value is used to set the OC7 PWM duty cycle.

Pseudocode:
\code
Initialize Ports
Initialize delayCounter
do forever
  decrement delayCounter
  if delayCounter == 0
    toggle LEDs D4-D9
    Increment OC3 duty cycle
    if duty cycle > max
      toggle LED D10
      reset duty cycle
  if potValue < lowValue
    lowValue = potValue
  if potValue > highValue
    highValue = potValue
  set OC7 duty cycle based on potValue
end do
\endcode
*/
int main(int argc, char** argv) {
    int delayCounter;

    Initialize();
    highValue = 0;
    lowValue=65535;

    // Give a short wait befre actually starting
    delayCounter=COUNTERMAX;
    while ( delayCounter-- )
        ;
    delayCounter=COUNTERMAX;
    while(1) {
        delayCounter--;
        if ( !delayCounter )
        {
            LATA ^= 0x7e;           // Toggle LEDs D4-D9
            delayCounter=COUNTERMAX;
            OC3RS += OC3DELTA;      // Increment duty cycle
            if ( OC3RS > OC3MAX )   // If almost always on
            {
                LATA ^= 0x80;       //Toggle LED D10
                OC3RS = OC3INIT;    // Reset duty cycle
            }
        }
        if ( potValue < lowValue )
            lowValue = potValue;
        if ( potValue > highValue )
            highValue = potValue;
        OC7RS = potValue/2;         // Set OC7 duty cycle to analog value
    }
    return (EXIT_SUCCESS);
}

