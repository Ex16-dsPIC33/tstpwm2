/*! \file T1Initialize.c
    \brief Initialize timer 1
*/
#include <p33Fxxxx.h>
#include "Constants.h"

/*! Initialize timer 1
 *
 * Set timer 1 to interrupt periodically.  This will be use
 * to flash the Explorer 16 LEDs.
 *
 * Pseudocode:
 * \code
 *   Clear Timer 1 counter
 *   Set Timer 1 timeout value
 *   Clear the Timer 1 interrupt flag
 *   Set the Timer 1 interrupt bit
 *   Set Timer 1 to use system clock, 1:256 prescale, start timer
 * \endcode
 */
void T1Initialize( void )
{
// Timer used to flash LED in interrupt context
    TMR1 = 0;             // clear timer 1
    PR1 = PR1MAX;         // interrupt every PR1MAX ticks
    IFS0bits.T1IF = 0;    // clr interrupt flag
    IEC0bits.T1IE = 1;    // set interrupt enable bit
    T1CON = 0x8030;       // Fosc/4, 1:256 prescale, start TMR1
}
