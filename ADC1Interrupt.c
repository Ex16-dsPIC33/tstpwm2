/*! \file ADC1Interrupt.c

 \brief Interrupt service routing for the ADC

 This file provides the (very simple) ISR that is executed
 whenever an analog conversion has completed.
*/
#include <p33Fxxxx.h>

// A/D Converter interrupt service routine

extern unsigned int potValue;   // Pass result to global

//! ADC1 Interrupt Service Routine
/*!
  Pseudocode
  \code
    Clear the interrupt flag
    Grab the analog value and store it in potValue
  \endcode
*/
void __attribute__((__interrupt__, auto_psv)) _ADC1Interrupt( void )
{
  IFS0bits.AD1IF = 0;    // Clear A/D interrupt flag
  potValue = ADC1BUF0;   // Save the potentiometer value
}
