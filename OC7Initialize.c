/*! \file OC7Initialize.c
    \brief Initialize the OC7 PWM device
*/
#include <p33Fxxxx.h>

/*! Initialize OC7 for PWM
 * Timer 2 is set up and the PWM on OC7 is initialized
 * to use Timer 2.
 *
 * Pseudocode:
 * \code
 *   Clear TMR2
 *   Set the Timer 2 counter to 1000
 *   Set the initial PWM 7 duty cycle to 999
 *   Set up PWM 7 (OC7) to use Timer 2
 *   Set Timer 2 to 1:4 prescaler, use clock and start the timer
 * \endcode
 */
void OC7Initialize( void )
{
    // Set up PWM on OC7 (RD6)
    TMR2 = 0;             // Clear timer 2
    PR2 = 1000;           // Timer 2 counter to 1000
    OC7RS = 999;          // PWM 7 almost to PR2
    OC7R = 999;           //   sets duty cycle almost zero
    OC7CON = 0x6;         // Set OC 7 to PWM mode, timer 2
    T2CON = 0x8010;       // Fosc/4, 1:4 prescale, start TMR2
}
