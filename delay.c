/*! \file delay.c
    \brief Delay timer
*/
#include "Constants.h"

/*! Delay timer
 * Waste time by counting down from DELAYLOOPCOUNT
 */
void delay(void) {
    long i = DELAYLOOPCOUNT;
    while(i--)
        ;
}
