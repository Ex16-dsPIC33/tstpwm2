/*! \file Initialize.c
    \brief Initialization
*/

#include <p33Fxxxx.h>

/*!
 * Initialize() initializes all the involved peripherals
 * by calling the appropriate routine.
 * 
 * Pseudocode:
 *\code
 *   Initialize ports
 *   Initialize Timer 1
 *   Initialize PWM 7
 *   Initialize PWM 3
 *   Initialize the A/D Converter
 *\endcode
 */
void Initialize( void )
{
    PortInitialize();
    T1Initialize();
    OC7Initialize();
    OC3Initialize();
    ADC1Initialize();

}
