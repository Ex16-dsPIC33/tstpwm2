/*! \file Constants.h
  \brief Contains manifest constants for tstPWM2.X
*/

//! Delay loop counter
#define DELAYLOOPCOUNT  64000
//! Internal delay counter
#define COUNTERMAX      50000   
//! Initial value for OC3
#define OC3INIT         0       
//! Max value for OC3
#define OC3MAX          990     
//! Increment value for OC3
#define OC3DELTA        10      
//! Initial value for Timer 1
#define PR1MAX          20000   
