/*! \file PortInitialize.c
    \brief Inititlize I/O ports
*/
#include <p33Fxxxx.h>

/*! Initilization
 * 
 * Set appropriate I/O pins to outputs and initialize
 * their states.
 *
 * Pseudocode:
 * \code
 *   Set LATA state
 *   Set LATA 0-7 to outputs
 *   Set LATD state
 *   Set PORTB 2 and 6 to outputs
 * \endcode
 */
void PortInitialize( void )
{
    /* set LEDs (D3-D10/RA0-RA7) alternate LEDs on */
    LATA = 0xFF55;
    /* set LED pins (D3-D10/RA0-RA7) as outputs */
    TRISA = 0xFF00;
    /* Set proto board LEDS all off */
    LATD = 0xFFFF;
    /* set proto board LEDS RD2 & RD6 as outputs */
    TRISD = 0xFFBB;
}
