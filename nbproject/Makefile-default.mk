#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile

# Environment
MKDIR=mkdir -p
RM=rm -f 
CP=cp 
# Macros
CND_CONF=default

ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf
else
IMAGE_TYPE=production
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf
endif
# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}
# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files
OBJECTFILES=${OBJECTDIR}/ADC1Interrupt.o ${OBJECTDIR}/ADC1Initialize.o ${OBJECTDIR}/delay.o ${OBJECTDIR}/OC3Initialize.o ${OBJECTDIR}/OC7Initialize.o ${OBJECTDIR}/Initialize.o ${OBJECTDIR}/main.o ${OBJECTDIR}/T1Interrupt.o ${OBJECTDIR}/PortInitialize.o ${OBJECTDIR}/T1Initialize.o


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

# Path to java used to run MPLAB X when this makefile was created
MP_JAVA_PATH=/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0/jre/bin/
OS_ORIGINAL="Linux"
OS_CURRENT="$(shell uname -s)"
############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
MP_CC=/opt/microchip/mplabc30/v3.24/bin/pic30-gcc
# MP_BC is not defined
MP_AS=/opt/microchip/mplabc30/v3.24/bin/pic30-as
MP_LD=/opt/microchip/mplabc30/v3.24/bin/pic30-ld
MP_AR=/opt/microchip/mplabc30/v3.24/bin/pic30-ar
# MP_BC is not defined
MP_CC_DIR=/opt/microchip/mplabc30/v3.24/bin
# MP_BC_DIR is not defined
MP_AS_DIR=/opt/microchip/mplabc30/v3.24/bin
MP_LD_DIR=/opt/microchip/mplabc30/v3.24/bin
MP_AR_DIR=/opt/microchip/mplabc30/v3.24/bin
# MP_BC_DIR is not defined
.build-conf: ${BUILD_SUBPROJECTS}
ifneq ($(OS_CURRENT),$(OS_ORIGINAL))
	@echo "***** WARNING: This make file contains OS dependent code. The OS this makefile is being run is different from the OS it was created in."
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf

MP_PROCESSOR_OPTION=33FJ256GP710
MP_LINKER_FILE_OPTION=,-Tp33FJ256GP710.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/ADC1Interrupt.o: ADC1Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/ADC1Interrupt.o.d -o ${OBJECTDIR}/ADC1Interrupt.o ADC1Interrupt.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/ADC1Interrupt.o.d > ${OBJECTDIR}/ADC1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${CP} ${OBJECTDIR}/ADC1Interrupt.o.tmp ${OBJECTDIR}/ADC1Interrupt.o.d 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/ADC1Interrupt.o.d > ${OBJECTDIR}/ADC1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${CP} ${OBJECTDIR}/ADC1Interrupt.o.tmp ${OBJECTDIR}/ADC1Interrupt.o.d 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.tmp
endif
${OBJECTDIR}/ADC1Initialize.o: ADC1Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/ADC1Initialize.o.d -o ${OBJECTDIR}/ADC1Initialize.o ADC1Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/ADC1Initialize.o.d > ${OBJECTDIR}/ADC1Initialize.o.tmp
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${CP} ${OBJECTDIR}/ADC1Initialize.o.tmp ${OBJECTDIR}/ADC1Initialize.o.d 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/ADC1Initialize.o.d > ${OBJECTDIR}/ADC1Initialize.o.tmp
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${CP} ${OBJECTDIR}/ADC1Initialize.o.tmp ${OBJECTDIR}/ADC1Initialize.o.d 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.tmp
endif
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/delay.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/delay.o.d -o ${OBJECTDIR}/delay.o delay.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/delay.o.d > ${OBJECTDIR}/delay.o.tmp
	${RM} ${OBJECTDIR}/delay.o.d 
	${CP} ${OBJECTDIR}/delay.o.tmp ${OBJECTDIR}/delay.o.d 
	${RM} ${OBJECTDIR}/delay.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/delay.o.d > ${OBJECTDIR}/delay.o.tmp
	${RM} ${OBJECTDIR}/delay.o.d 
	${CP} ${OBJECTDIR}/delay.o.tmp ${OBJECTDIR}/delay.o.d 
	${RM} ${OBJECTDIR}/delay.o.tmp
endif
${OBJECTDIR}/OC3Initialize.o: OC3Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/OC3Initialize.o.d -o ${OBJECTDIR}/OC3Initialize.o OC3Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/OC3Initialize.o.d > ${OBJECTDIR}/OC3Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${CP} ${OBJECTDIR}/OC3Initialize.o.tmp ${OBJECTDIR}/OC3Initialize.o.d 
	${RM} ${OBJECTDIR}/OC3Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/OC3Initialize.o.d > ${OBJECTDIR}/OC3Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${CP} ${OBJECTDIR}/OC3Initialize.o.tmp ${OBJECTDIR}/OC3Initialize.o.d 
	${RM} ${OBJECTDIR}/OC3Initialize.o.tmp
endif
${OBJECTDIR}/OC7Initialize.o: OC7Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/OC7Initialize.o.d -o ${OBJECTDIR}/OC7Initialize.o OC7Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/OC7Initialize.o.d > ${OBJECTDIR}/OC7Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${CP} ${OBJECTDIR}/OC7Initialize.o.tmp ${OBJECTDIR}/OC7Initialize.o.d 
	${RM} ${OBJECTDIR}/OC7Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/OC7Initialize.o.d > ${OBJECTDIR}/OC7Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${CP} ${OBJECTDIR}/OC7Initialize.o.tmp ${OBJECTDIR}/OC7Initialize.o.d 
	${RM} ${OBJECTDIR}/OC7Initialize.o.tmp
endif
${OBJECTDIR}/Initialize.o: Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/Initialize.o.d -o ${OBJECTDIR}/Initialize.o Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/Initialize.o.d > ${OBJECTDIR}/Initialize.o.tmp
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${CP} ${OBJECTDIR}/Initialize.o.tmp ${OBJECTDIR}/Initialize.o.d 
	${RM} ${OBJECTDIR}/Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/Initialize.o.d > ${OBJECTDIR}/Initialize.o.tmp
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${CP} ${OBJECTDIR}/Initialize.o.tmp ${OBJECTDIR}/Initialize.o.d 
	${RM} ${OBJECTDIR}/Initialize.o.tmp
endif
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/main.o.d -o ${OBJECTDIR}/main.o main.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/main.o.d > ${OBJECTDIR}/main.o.tmp
	${RM} ${OBJECTDIR}/main.o.d 
	${CP} ${OBJECTDIR}/main.o.tmp ${OBJECTDIR}/main.o.d 
	${RM} ${OBJECTDIR}/main.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/main.o.d > ${OBJECTDIR}/main.o.tmp
	${RM} ${OBJECTDIR}/main.o.d 
	${CP} ${OBJECTDIR}/main.o.tmp ${OBJECTDIR}/main.o.d 
	${RM} ${OBJECTDIR}/main.o.tmp
endif
${OBJECTDIR}/T1Interrupt.o: T1Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/T1Interrupt.o.d -o ${OBJECTDIR}/T1Interrupt.o T1Interrupt.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/T1Interrupt.o.d > ${OBJECTDIR}/T1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${CP} ${OBJECTDIR}/T1Interrupt.o.tmp ${OBJECTDIR}/T1Interrupt.o.d 
	${RM} ${OBJECTDIR}/T1Interrupt.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/T1Interrupt.o.d > ${OBJECTDIR}/T1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${CP} ${OBJECTDIR}/T1Interrupt.o.tmp ${OBJECTDIR}/T1Interrupt.o.d 
	${RM} ${OBJECTDIR}/T1Interrupt.o.tmp
endif
${OBJECTDIR}/PortInitialize.o: PortInitialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/PortInitialize.o.d -o ${OBJECTDIR}/PortInitialize.o PortInitialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/PortInitialize.o.d > ${OBJECTDIR}/PortInitialize.o.tmp
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${CP} ${OBJECTDIR}/PortInitialize.o.tmp ${OBJECTDIR}/PortInitialize.o.d 
	${RM} ${OBJECTDIR}/PortInitialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/PortInitialize.o.d > ${OBJECTDIR}/PortInitialize.o.tmp
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${CP} ${OBJECTDIR}/PortInitialize.o.tmp ${OBJECTDIR}/PortInitialize.o.d 
	${RM} ${OBJECTDIR}/PortInitialize.o.tmp
endif
${OBJECTDIR}/T1Initialize.o: T1Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/T1Initialize.o.d -o ${OBJECTDIR}/T1Initialize.o T1Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/T1Initialize.o.d > ${OBJECTDIR}/T1Initialize.o.tmp
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${CP} ${OBJECTDIR}/T1Initialize.o.tmp ${OBJECTDIR}/T1Initialize.o.d 
	${RM} ${OBJECTDIR}/T1Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/T1Initialize.o.d > ${OBJECTDIR}/T1Initialize.o.tmp
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${CP} ${OBJECTDIR}/T1Initialize.o.tmp ${OBJECTDIR}/T1Initialize.o.d 
	${RM} ${OBJECTDIR}/T1Initialize.o.tmp
endif
else
${OBJECTDIR}/ADC1Interrupt.o: ADC1Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/ADC1Interrupt.o.d -o ${OBJECTDIR}/ADC1Interrupt.o ADC1Interrupt.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/ADC1Interrupt.o.d > ${OBJECTDIR}/ADC1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${CP} ${OBJECTDIR}/ADC1Interrupt.o.tmp ${OBJECTDIR}/ADC1Interrupt.o.d 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/ADC1Interrupt.o.d > ${OBJECTDIR}/ADC1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.d 
	${CP} ${OBJECTDIR}/ADC1Interrupt.o.tmp ${OBJECTDIR}/ADC1Interrupt.o.d 
	${RM} ${OBJECTDIR}/ADC1Interrupt.o.tmp
endif
${OBJECTDIR}/ADC1Initialize.o: ADC1Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/ADC1Initialize.o.d -o ${OBJECTDIR}/ADC1Initialize.o ADC1Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/ADC1Initialize.o.d > ${OBJECTDIR}/ADC1Initialize.o.tmp
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${CP} ${OBJECTDIR}/ADC1Initialize.o.tmp ${OBJECTDIR}/ADC1Initialize.o.d 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/ADC1Initialize.o.d > ${OBJECTDIR}/ADC1Initialize.o.tmp
	${RM} ${OBJECTDIR}/ADC1Initialize.o.d 
	${CP} ${OBJECTDIR}/ADC1Initialize.o.tmp ${OBJECTDIR}/ADC1Initialize.o.d 
	${RM} ${OBJECTDIR}/ADC1Initialize.o.tmp
endif
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/delay.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/delay.o.d -o ${OBJECTDIR}/delay.o delay.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/delay.o.d > ${OBJECTDIR}/delay.o.tmp
	${RM} ${OBJECTDIR}/delay.o.d 
	${CP} ${OBJECTDIR}/delay.o.tmp ${OBJECTDIR}/delay.o.d 
	${RM} ${OBJECTDIR}/delay.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/delay.o.d > ${OBJECTDIR}/delay.o.tmp
	${RM} ${OBJECTDIR}/delay.o.d 
	${CP} ${OBJECTDIR}/delay.o.tmp ${OBJECTDIR}/delay.o.d 
	${RM} ${OBJECTDIR}/delay.o.tmp
endif
${OBJECTDIR}/OC3Initialize.o: OC3Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/OC3Initialize.o.d -o ${OBJECTDIR}/OC3Initialize.o OC3Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/OC3Initialize.o.d > ${OBJECTDIR}/OC3Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${CP} ${OBJECTDIR}/OC3Initialize.o.tmp ${OBJECTDIR}/OC3Initialize.o.d 
	${RM} ${OBJECTDIR}/OC3Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/OC3Initialize.o.d > ${OBJECTDIR}/OC3Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC3Initialize.o.d 
	${CP} ${OBJECTDIR}/OC3Initialize.o.tmp ${OBJECTDIR}/OC3Initialize.o.d 
	${RM} ${OBJECTDIR}/OC3Initialize.o.tmp
endif
${OBJECTDIR}/OC7Initialize.o: OC7Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/OC7Initialize.o.d -o ${OBJECTDIR}/OC7Initialize.o OC7Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/OC7Initialize.o.d > ${OBJECTDIR}/OC7Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${CP} ${OBJECTDIR}/OC7Initialize.o.tmp ${OBJECTDIR}/OC7Initialize.o.d 
	${RM} ${OBJECTDIR}/OC7Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/OC7Initialize.o.d > ${OBJECTDIR}/OC7Initialize.o.tmp
	${RM} ${OBJECTDIR}/OC7Initialize.o.d 
	${CP} ${OBJECTDIR}/OC7Initialize.o.tmp ${OBJECTDIR}/OC7Initialize.o.d 
	${RM} ${OBJECTDIR}/OC7Initialize.o.tmp
endif
${OBJECTDIR}/Initialize.o: Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/Initialize.o.d -o ${OBJECTDIR}/Initialize.o Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/Initialize.o.d > ${OBJECTDIR}/Initialize.o.tmp
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${CP} ${OBJECTDIR}/Initialize.o.tmp ${OBJECTDIR}/Initialize.o.d 
	${RM} ${OBJECTDIR}/Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/Initialize.o.d > ${OBJECTDIR}/Initialize.o.tmp
	${RM} ${OBJECTDIR}/Initialize.o.d 
	${CP} ${OBJECTDIR}/Initialize.o.tmp ${OBJECTDIR}/Initialize.o.d 
	${RM} ${OBJECTDIR}/Initialize.o.tmp
endif
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/main.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/main.o.d -o ${OBJECTDIR}/main.o main.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/main.o.d > ${OBJECTDIR}/main.o.tmp
	${RM} ${OBJECTDIR}/main.o.d 
	${CP} ${OBJECTDIR}/main.o.tmp ${OBJECTDIR}/main.o.d 
	${RM} ${OBJECTDIR}/main.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/main.o.d > ${OBJECTDIR}/main.o.tmp
	${RM} ${OBJECTDIR}/main.o.d 
	${CP} ${OBJECTDIR}/main.o.tmp ${OBJECTDIR}/main.o.d 
	${RM} ${OBJECTDIR}/main.o.tmp
endif
${OBJECTDIR}/T1Interrupt.o: T1Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/T1Interrupt.o.d -o ${OBJECTDIR}/T1Interrupt.o T1Interrupt.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/T1Interrupt.o.d > ${OBJECTDIR}/T1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${CP} ${OBJECTDIR}/T1Interrupt.o.tmp ${OBJECTDIR}/T1Interrupt.o.d 
	${RM} ${OBJECTDIR}/T1Interrupt.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/T1Interrupt.o.d > ${OBJECTDIR}/T1Interrupt.o.tmp
	${RM} ${OBJECTDIR}/T1Interrupt.o.d 
	${CP} ${OBJECTDIR}/T1Interrupt.o.tmp ${OBJECTDIR}/T1Interrupt.o.d 
	${RM} ${OBJECTDIR}/T1Interrupt.o.tmp
endif
${OBJECTDIR}/PortInitialize.o: PortInitialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/PortInitialize.o.d -o ${OBJECTDIR}/PortInitialize.o PortInitialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/PortInitialize.o.d > ${OBJECTDIR}/PortInitialize.o.tmp
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${CP} ${OBJECTDIR}/PortInitialize.o.tmp ${OBJECTDIR}/PortInitialize.o.d 
	${RM} ${OBJECTDIR}/PortInitialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/PortInitialize.o.d > ${OBJECTDIR}/PortInitialize.o.tmp
	${RM} ${OBJECTDIR}/PortInitialize.o.d 
	${CP} ${OBJECTDIR}/PortInitialize.o.tmp ${OBJECTDIR}/PortInitialize.o.d 
	${RM} ${OBJECTDIR}/PortInitialize.o.tmp
endif
${OBJECTDIR}/T1Initialize.o: T1Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF ${OBJECTDIR}/T1Initialize.o.d -o ${OBJECTDIR}/T1Initialize.o T1Initialize.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/T1Initialize.o.d > ${OBJECTDIR}/T1Initialize.o.tmp
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${CP} ${OBJECTDIR}/T1Initialize.o.tmp ${OBJECTDIR}/T1Initialize.o.d 
	${RM} ${OBJECTDIR}/T1Initialize.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/T1Initialize.o.d > ${OBJECTDIR}/T1Initialize.o.tmp
	${RM} ${OBJECTDIR}/T1Initialize.o.d 
	${CP} ${OBJECTDIR}/T1Initialize.o.tmp ${OBJECTDIR}/T1Initialize.o.d 
	${RM} ${OBJECTDIR}/T1Initialize.o.tmp
endif
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf  -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -o dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf ${OBJECTFILES}        -Wl,--defsym=__MPLAB_BUILD=1,--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__ICD2RAM=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD2=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf  -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf ${OBJECTFILES}        -Wl,--defsym=__MPLAB_BUILD=1,--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}/pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/tstPWM2.X.${IMAGE_TYPE}.elf -omf=elf
endif


# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf:
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
